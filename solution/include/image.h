#pragma once
#ifndef H_image
#define H_image

#include  <stdint.h>
#include <stdlib.h>
#include  <stdio.h>

struct pixel { uint8_t b, g, r; };

struct image {
	uint32_t width, height;
	struct pixel* data;
};

void image_rotate_180(struct image* img);
void image_rotate_p90(struct image* img);
void image_rotate_n90(struct image* img);

enum rotate_result {
    ROTATE_OK = 0,
    INVALID_ANGLE
};

enum rotate_result image_rotate(struct image* img, int angle);
#endif

