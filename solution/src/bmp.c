#include "bmp.h"

enum read_status read_bmp(FILE* in, struct image* img) {
	struct bmp_header hdr;

	fseek(in, 0, SEEK_END);
	uint32_t fsize = ftell(in);
	rewind(in);

	if (!fread(&hdr, sizeof(hdr), 1, in)) {
		return READ_FAIL;
	}
	if (fsize != hdr.bfileSize) {
		return READ_INVALID_SIGNATURE;
	}

	if (hdr.biSize != 40) {
		return READ_INVALID_HEADER;
	}
	if ((hdr.biBitCount != 24) || (hdr.biCompression != 0)) {
		return READ_INVALID_BITS;
	}

	img->data = malloc(hdr.biWidth * BYTES_PER_PIXEL*hdr.biHeight);

	if (img->data == NULL){
		printf("Insufficient memory available\n");
		return READ_FAIL;
	};

	img->height = hdr.biHeight;
	img->width = hdr.biWidth;
	int linePadding = 0;
	if (hdr.biWidth* BYTES_PER_PIXEL % 4 != 0) {
		linePadding = (int)(4 - (hdr.biWidth * BYTES_PER_PIXEL) % 4);
	}

	fseek(in, hdr.bOffBits, SEEK_SET);
	for (uint32_t i = 1; i <= img->height; ++i)
	{
		fread(&img->data[(hdr.biHeight - i) * hdr.biWidth], BYTES_PER_PIXEL, hdr.biWidth, in);
		fseek(in, linePadding, SEEK_CUR);
	}

	return READ_OK;
}

enum write_status write_bmp(FILE* out, struct image* img) {
	struct bmp_header hdr;
	int linePadding = 0;
	if (img->width * BYTES_PER_PIXEL % 4 != 0) {
		linePadding = (int)(4 - (img->width * BYTES_PER_PIXEL) % 4);
	}


	memcpy(&hdr.bfType,"BM",sizeof("BM"));
	hdr.bfReserved = 0;
	hdr.bfileSize = (img->width* BYTES_PER_PIXEL+ linePadding) * img->height + 54;
	hdr.bOffBits = 54;

	hdr.biSize = 40;
	hdr.biWidth = img->width;
	hdr.biHeight = img->height;
	hdr.biPlanes = 1;
	hdr.biBitCount = BYTES_PER_PIXEL*8;
	hdr.biCompression = 0;
	hdr.biSizeImage = img->width * img->height * BYTES_PER_PIXEL;
	hdr.biXPelsPerMeter = 2834;
	hdr.biYPelsPerMeter = 2834;
	hdr.biClrUsed=0;
	hdr.biClrImportant = 0;
	if (!fwrite(&hdr, sizeof(hdr), 1, out)) {
		return WRITE_ERROR;
	}

	if (linePadding == 0) {
		return WRITE_ERROR;
	}
	
	char* pad = malloc(linePadding);
	for (uint32_t i = 1; i <= img->height; ++i)
	{
		int pixelOffset = (int)(img->height - i) * (int)img->width;
		if (fwrite(&img->data[pixelOffset], BYTES_PER_PIXEL, img->width, out)< img->width) {
			free(pad);
			return WRITE_ERROR;
		}
		if (fwrite(pad, 1, linePadding, out) < linePadding) {
			free(pad);
			return WRITE_ERROR;
		}
	}
	free(pad);
	return WRITE_OK;
}

