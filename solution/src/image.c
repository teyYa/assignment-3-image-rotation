#include "image.h"

void image_rotate_180(struct image* img) {
	struct pixel* n_img;
	n_img= malloc(img->height * img->width * sizeof(struct pixel));
	for (uint32_t i = 0; i < img->height; ++i)
		for (uint32_t j = 0; j < img->width; ++j) {
			n_img[(img->height -i-1)* img->width +(img->width -j-1)] = img->data[i * img->width +j];
		}
	free(img->data);
	img->data = n_img;
}

void image_rotate_p90(struct image* img) {
	struct image n_img;
	n_img.data = malloc(img->height * img->width * sizeof(struct pixel));
	n_img.height = img->width;
	n_img.width = img->height;
	for (uint32_t i = 0; i < n_img.height; ++i)
		for (uint32_t j = 0; j < n_img.width; ++j) {
			n_img.data[i * n_img.width + j] = img->data[(img->height-j-1) * img->width + i];
		}
	free(img->data);
	img->width = n_img.width;
	img->height=n_img.height;
	img->data = n_img.data;
}

void image_rotate_n90(struct image* img) {
	struct image n_img;
	n_img.data = malloc(img->height * img->width * sizeof(struct pixel));
	n_img.height = img->width;
	n_img.width = img->height;
	for (uint32_t i = 0; i < n_img.height; ++i)
		for (uint32_t j = 0; j < n_img.width; ++j) {
			n_img.data[(n_img.height-i-1) * n_img.width + j] = img->data[j  * img->width + i];
		}
	free(img->data);
	img->width = n_img.width;
	img->height = n_img.height;
	img->data = n_img.data;
}

enum rotate_result image_rotate(struct image* img, int angle) {
	(void)img;(void)angle;
	if (angle % 90 != 0) {
		return INVALID_ANGLE;
	}
	int angle_remain = angle % 360;
	if (angle_remain / 180 != 0) {
		image_rotate_180(img);
		angle_remain = angle_remain % 180;
	}
	if (angle_remain == 0) {
		return ROTATE_OK;
	}
	if (angle_remain > 0) {
		image_rotate_p90(img);
	}
	else {
		image_rotate_n90(img);
	}
	printf("%i\n", angle_remain);
	return ROTATE_OK;
}

