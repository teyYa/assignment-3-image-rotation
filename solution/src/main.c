#include <stdio.h>

#include "bmp.h"
#include "image.h"

int main( int argc, char** argv ) {
    if (argc != 4)
    {
        printf("Unexpected number of arguments\n");
        return -1;
    }
    char* source_image = argv[1];
    char* transformed_image = argv[2];
    char *angle = argv[3];


    FILE* f_bmp;
    if ((f_bmp = fopen(source_image, "rb")) == NULL) {
        printf("Cannot open input file.\n");
        return -1;
    }

    struct image img;
    int res = read_bmp(f_bmp, &img);
    if (res != READ_OK) {
        printf("Error reading BMP. Error code %i\n", res);
        return -2;
    }

    fclose(f_bmp);

    if (image_rotate(&img, atoi(angle))) {
        printf("Error rotating image. Check angle\n");
        return -3;
    }

    if ((f_bmp = fopen(transformed_image, "wb")) == NULL) {
        printf("Cannot open output file.\n");
        return -4;
    }
    int res_w = write_bmp(f_bmp, &img);
    if (res_w != WRITE_OK) {
        printf("Error writing BMP. Error code %i\n", res_w);
        return -5;
    }
    fclose(f_bmp);

    return 0;
}
